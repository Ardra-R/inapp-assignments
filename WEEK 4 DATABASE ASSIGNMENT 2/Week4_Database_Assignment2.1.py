import sqlite3

#connection object to represent our database
conn = sqlite3.connect(':memory:')

#cursor to execute sql commands
c = conn.cursor()

#creating employee table
c.execute("""CREATE TABLE employee(id integer PRIMARY KEY,
                                   name text,
                                   salary integer,
                                   department_id integer)""")

#adding a new column to the table
c.execute("ALTER TABLE employee ADD COLUMN city char(25)")

#inserting 5 record into the table
emp_info = [('1','Tom','30000','101','Jaipur'),
            ('2','Kim','20000','102','Berlin'),
            ('3','Ann','35000','103','Kollam'),
            ('4','Jay','40000','104','London'),
            ('5','Eva','50000','105','Paris'),]
c.executemany("INSERT INTO employee VALUES (?,?,?,?,?)", emp_info)

#reading name, id and salary and printing it
def printin():
    c.execute("SELECT name,id,salary FROM employee")
    item = c.fetchall()
    print('The name, id and salary of employees are given below.\n')
    print('NAME'+'\t'+'ID'+'\t'+'SALARY\n','------------------------')
    for i in item:
        print(i[0],'\t',i[1],'\t',i[2])

#Print the details of employees whose names start with ‘j’ (or any letter input by the user)
def nameinfo():
    c.execute("SELECT name FROM employee")
    l = c.fetchall()
    list = []
    for item in l: list.append(item[0][0])
    return list

def empname(letter):
    if letter in nameinfo():
        c.execute("SELECT name FROM employee")
        l = c.fetchall()
        for i in l:
            if letter == i[0][0]:
                letter = i[0]
                break
            else:continue
        c.execute("SELECT * FROM employee WHERE name = ? ",(letter,))
        items = c.fetchall()
        print('ID'+'\t'+'NAME'+'\t'+'SALARY'+'\t '+'DEPARTMENT ID\n','------------------------------------------------')
        for item in items:
            print(item[0],'\t',item[1],'\t',item[2],'\t',item[3])
    else:
        if letter not in nameinfo():
            print(f'There is no employee with name starting with {letter}')
        else:
            print('THe details of employee wuth name starting with the letter J is given.')
            c.execute("SELECT * FROM employee WHERE name LIKE 'j%'")
            items = c.fetchall()
            print('ID'+'\t'+'NAME'+'\t'+'SALARY'+'\t '+'DEPARTMENT ID\n','------------------------------------------------')
            for item in items:
                print(item[0],'\t',item[1],'\t',item[2],'\t',item[3])

#Print the details of employees with ID’s inputted by the user.
def empid(id):
    c.execute("SELECT *FROM employee WHERE id=?",id)
    items= c.fetchall()
    print('\nID'+'\t'+'NAME'+'\t'+'SALARY'+'\t '+'DEPARTMENT ID\n','------------------------------------------------')
    for item in items:
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3])

#Change the name of the employee whose ID is input by the user.             
def changename(idd):
    c.execute("SELECT id FROM employee")
    #l = c.fetchall()
    #if (idd in l):
    n = input('\nENter the new name you want to give the employee:')
    c.execute("UPDATE employee SET name=? WHERE id=?",(n,idd))
    c.execute("SELECT * FROM employee WHERE id=?",(idd,))
    l = c.fetchall()
    print('\n')
    print('ID'+'\t'+'NAME'+'\t'+'SALARY'+'\t '+'DEPARTMENT ID\n','------------------------------------------------')
    for item in l:
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3])

#main program

printin()
i= input('\nDo you want to see the details of employees with name starting with the letter of your choice?(Type yes or no)\n ')
if(i == 'yes'):
    letter = input('\nEnter any letter from the following:\nT, K, A, J, E\n').upper()
    empname(letter)
else: pass
id = (input('\nEnter any of the given ID to see employee details:\n1,2,3,4,5\n'))
empid(id)
u = int(input('\nDo you want to change the name of the employee with the selected ID. \nType:\n1- to change\n0-do not change\n'))
if u==1:
    changename(id)
else: pass
    
conn.commit()
