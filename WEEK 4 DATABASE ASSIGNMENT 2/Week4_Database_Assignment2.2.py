import sqlite3

#connection object to represent our database
conn = sqlite3.connect(':memory:')

#cursor to execute sql commands
c = conn.cursor()

#Create one more Table called Departments with two columns Department_id and Department_name.
c.execute("""CREATE TABLE departments(department_id integer PRIMARY KEY,
                                      department_name text)""")

#inserting 5 record into the table
dept = [('1','General'),
        ('2','Marketing'),
        ('3','Finance'),
        ('4','Operations'),
        ('5','Purachase'),]   

c.executemany("INSERT INTO departments VALUES (?,?)", dept)

#creating employee table
c.execute("""CREATE TABLE employee(id integer PRIMARY KEY,
                                   name text,
                                   salary integer,
                                   department_id integer)""")

#adding a new column to the table
c.execute("ALTER TABLE employee ADD COLUMN city char(25)")

#inserting 5 record into the table
emp_info = [('1','Tom','30000','101','Jaipur'),
            ('2','Kim','20000','102','Berlin'),
            ('3','Ann','35000','103','Kollam'),
            ('4','Jay','40000','104','London'),
            ('5','Eva','50000','105','Paris'),]
c.executemany("INSERT INTO employee VALUES (?,?,?,?,?)", emp_info)


def employeeinfo(k):
    c.execute("SELECT * FROM employee WHERE department_id = ? ",(k,))
    emp = c.fetchall()
    print(emp)
    c.execute("SELECT * FROM departments WHERE department_id = ? ",(k,))
    d = c.fetchall()
    print('\n')
    print('ID'+'\t'+'NAME'+'\t'+'SALARY'+'\t '+'DEPARTMENT ID'+'\t'+'DEPARTMENT NAME\n')
    print('------------------------------------------------------------------------------------------------')
    for item  in emp:
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3])



#Print the details of all the employees in a particular department (Department_id is input by the user)

k = input('To find details of employees in a department, enter department_id.\nThese are the available department_ids\n1  2  3  4  5\n')
employeeinfo(k)
conn.commit()
conn.close()