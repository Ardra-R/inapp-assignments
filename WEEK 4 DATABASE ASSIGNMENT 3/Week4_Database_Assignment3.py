import sqlite3
conn = sqlite3.connect("database.sqlite")
c = conn.cursor()

#Print the names of both the Home Teams and Away Teams in each match played in 2015 and Full time Home Goals (FTHG) = 5
def aaa():
    print('Names of Home Teams and Away Teams in each match played in 2015 and Full time Home Goals (FTHG) = 5\n')
    c.execute("SELECT HomeTeam, AwayTeam FROM Matches WHERE Season = ? AND FTHG = ?",(2015,5))
    items = c.fetchall()
    print('HOME TEAMS'+'\t'+'AWAY TEAMS\n')
    print('-------------------------------------------')
    for item in items:
        print(item[0],'\t',item[1])


#Print the details of the matches where Arsenal is the Home Team and  Full Time Result (FTR) is “A” (Away Win)
def bbb():
    print("\nDetails of the matches where Arsenal is the Home Team and  Full Time Result is “A”\n")
    c.execute("SELECT * FROM Matches WHERE HomeTeam = ? AND FTR = ?",('Arsenal','A'))
    items = c.fetchall()
    print('Match_ID'+'\t'+'Div'+'\t'+'Season'+'\t'+'Date'+'\t'+'HomeTeam'+'\t'+'AwayTeam'+'\t'+'FTHG'+'\t'+'FTAG'+'       '+'FTR')
    print('-------------------------------------------------------------------------------------------------------------------------------------')
    for item in items:
        print('blee')
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3],'\t',item[4],'\t',item[5],'\t',item[6],'\t',item[7],'       ',item[8])

#Print all the matches from the 2012 season till the 2015 season where Away Team is Bayern Munich and Full time Away Goals (FTHG) > 2
def ccc():
    print('\nMatches from the 2012 season till 2015 season where Away Team is Bayern Munich and Full time Away Goals > 2\n')
    c.execute("SELECT * FROM Matches WHERE season BETWEEN ? AND ? AND AwayTeam = ? AND FTAG > ?",(2012,2015,'Bayern Munich',2))
    items = c.fetchall()
    print('Match_ID'+'\t'+'Div'+'\t'+'Season'+'\t'+'Date'+'\t'+'HomeTeam'+'\t'+'AwayTeam'+'\t'+'FTHG'+'\t'+'FTAG'+'\t'+'FTR')
    print('-------------------------------------------------------------------------------------------------------------------------------------')
    for item in items:
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3],'\t',item[4],'\t',item[5],'\t',item[6],'\t',item[7],'\t',item[8])

#Print all the matches where the Home Team name begins with “A” and Away Team name begins with “M”
def ddd():
    print('Matches where the Home Team name begins with “A” and Away Team name begins with “M”')
    c.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%' ")
    items = c.fetchall()
    print('Match_ID'+'\t'+'Div'+'\t'+'Season'+'\t'+'Date'+'\t'+'HomeTeam'+'\t'+'AwayTeam'+'\t'+'FTHG'+'\t'+'FTAG'+'\t'+'FTR')
    print('-------------------------------------------------------------------------------------------------------------------------------------')
    for item in items:
        print(item[0],'\t',item[1],'\t',item[2],'\t',item[3],'\t',item[4],'\t',item[5],'\t',item[6],'\t',item[7],'\t',item[8])


#Question 2
#Counts all the rows in the Teams table
def eee():
    c.execute("SELECT * FROM Teams")
    print('The number of rows in Team table are:')
    print(len(c.fetchall()))

#Print all the unique values that are included in the Season column in the Teams table
def fff():
    c.execute("SELECT DISTINCT Season FROM Teams")
    items= c.fetchall()
    print('Seasons\n-----------')
    for item in items:
        print(item[0])

#Print the largest and smallest stadium capacity that is included in the Teams table
def ggg():
    c.execute("SELECT MIN(StadiumCapacity) FROM Teams")
    print('The minimum stadium capacity is:')
    item = c.fetchall()
    for i in item:print(i[0])
    c.execute("SELECT MAX(StadiumCapacity) FROM Teams")
    print('The maximum stadium capacity is:')
    item = c.fetchall()
    for i in item:print(i[0])

#Print the sum of squad players for all teams during the 2014 season from the Teams table
def hhh():
    c.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season=2014")
    items = c.fetchall()
    print('Sum of squad players are:')
    for i in items:
        print(i[0])

#Query the Matches table to know how many goals did Man United score during home games on average?
def iii():
    c.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam = 'Man United' ")
    items = c.fetchall()
    print('Average of goals scored by Man United:')
    for i in items:
        print(i[0])


#Question 3
#a
def jjj():
    c.execute("SELECT HomeTeam, FTHG, FTAG FROM Matches WHERE Season = 2010 AND HomeTeam ='Aachen' ORDER BY FTHG DESC")
    items = c.fetchall()
    print('Home goals scored by team Aachen:\n')
    print('Home Team'+'\t'+'FTHG'+'\t'+'FTAF')
    for i in items:
        print(i[0],'\t\t',i[1],'\t',i[2])

#b
def kkk():
    c.execute("SELECT COUNT(*),HomeTeam FROM Matches WHERE Season= 2016 AND FTR='H' GROUP BY HomeTeam ORDER BY COUNT(*) DESC")
    items = c.fetchall()
    print('Total home game won by each teams in 2016 season:\n')
    print('Games Won'+'\t'+'Team Names\n')
    print('-------------------------------------------------')
    for item in items:
        print(item[0],'\t',item[1])

#c
def lll():
    c.execute("SELECT * FROM Unique_Teams LIMIT 10")
    items = c.fetchall()
    print('10 rows of Unique Teams Table:\n')
    print('Match_ID'+'\t\t'+'Unique_Teams_ID\n')
    print('-----------------------------------------')
    for i in items:
        print(i[0],'\t\t\t',i[1])

#d using where
def mmm():
    c.execute("SELECT * FROM Unique_Teams INNER JOIN Teams_in_Matches WHERE Unique_Teams.Unique_Team_ID= Teams_in_Matches.Unique_Team_ID ")
    items = c.fetchall()
    print('Match_ID'+'\t\t'+'Unique_Teams_ID'+'\t'+'Team Name'+'\t\t'+'Unique_Teams_ID\n')
    print('---------------------------------------------------')
    for i in items:
        print(i[0],'\t\t\t',i[1],'\t\t',i[2],'\t\t',i[3])

#d using join
def nnn():
    c.execute("SELECT * FROM Unique_Teams JOIN Teams_in_Matches ON Unique_Teams.Unique_Team_ID= Teams_in_Matches.Unique_Team_ID ")
    items = c.fetchall()
    print('Match_ID'+'\t\t'+'Unique_Teams_ID'+'\t'+'Team Name'+'\t\t'+'Unique_Teams_ID\n')
    print('---------------------------------------------------')
    for i in items:
        print(i[0],'\t\t\t',i[1],'\t\t',i[2],'\t\t',i[3])

#e
def ooo():
    c.execute("SELECT * FROM Unique_Teams JOIN Teams ON Unique_Teams.TeamName= Teams.TeamName LIMIT 10")
    items = c.fetchall()
    print('Team Name'+'\t\t'+'Unique_Teams_ID\n'+'\t\t'+'Season'+'\t\t'+'Team Name'+'\t\t'+'KaderHome'+'\t\t'+'AvgAgeHome'+'\t\t'+'ForeignPlayersHome'+'\t\t'+'AvgAgeHome'+'\t\t'+'OverallMarketValueHome'+'\t\t'+'AvgMarketValueHome'+'\t\t'+'StadiumCapacity')
    print('---------------------------------------------------')
    for i in items:
        print(i[0],'\t\t\t',i[1],'\t\t',i[2],'\t\t',i[3],'\t\t',i[4],'\t\t',i[5],'\t\t',i[6],'\t\t',i[7],'\t\t',i[8],'\t\t',i[9])

#f
def ppp():
    c.execute("SELECT *FROM Unique_Teams JOIN Teams ON Unique_Teams.TeamName= Teams.TeamName LIMIT 5 ")
    items = c.fetchall()
    print('TeamName'+'\t\t'+'Unique_Team_ID'+'\t\t'+ 'Season'+'\t\t'+ 'TeamName' +'\t\t'+'AvgAgeHome'+'\t\t'+'ForeignPlayersHome')
    print('----------------------------------------------------------------------------------------------------------------------------------')
    for i in items:
        print(i[0],'\t\t\t',i[1],'\t\t',i[2],'\t\t',i[3],'\t\t',i[4],'\t\t',i[5])
#g
def qqq():
    c.execute("SELECT MAX(Match_ID),Unique_Team_ID, * FROM Unique_Teams INNER JOIN Teams_in_Matches ON Unique_Teams.Unique_Team_ID=Teams_in_Matches.Unique_Team_ID WHERE Unique_Teams.Unique_Team_ID LIKE '%y' OR Unique_Teams.Unique_Team_ID LIKE '%r'  GROUP BY Unique_Team_ID")
    items= c.fetchall()
    print('Maximum Match_ID'+'\t\t'+'Unique_Team_ID'+'\t\t'+'Team Name'+'\t\t'+'Unique_Team_ID')
    for i in items:
        print(i[0],'\t\t\t',i[1],'\t\t',i[2],'\t\t',i[3],'\t\t',i[4])
#main 
aaa()
bbb()
ccc()
ddd()
eee()
fff()
ggg()
hhh()
iii()
jjj()
kkk()
lll()
mmm()
nnn()
ooo()
ppp()
qqq()
conn.commit()
conn.close()
