import random

import numpy as np

#Displaying board
def display_board(board):
    for item in board:
        print(item[0],'|',item[1],'|',item[2])

#Choosing symbol
def player_choice():
    choice = ''
    if (choice != 'x' or choice != 'o'):
        choice = input('Player 1, Enter your choice(x or o) ').lower()
    else:
        pass
    Pchoice=choice
    if Pchoice == 'x':Cchoice='o'
    else:Cchoice='x'
    return(Pchoice,Cchoice)

#Checking if position is occupied
def position_check(board,position):
    defaultpos={0:(0,0),1:(0,1),2:(0,2), 3:(1,0), 4:(1,1),5: (1,2),6: (2,0),7: (2,1),8: (2,2)}
    result = np.where(board == str(position))
    for indice in list(zip(result[0], result[1])):
        return indice==defaultpos[position]

#Choosing player position
def player_position(board):
    Pposition=int(input("Player 1, Enter your position on the board(0 to 8):"))
    if position_check(board,Pposition):
        return Pposition
    else:
        print('This position is occupied, please choose another position')
        player_position(board)


#Choosing computer position
def computer_position(board):
    Cposition=random.randint(0,9)
    while position_check(board,Cposition):
        return Cposition

#To place symbol in the position choosen 
def insertt(board,symbol,position):
    for i in range(len(board)):
        for j in range(len(board)):
            if(board[i][j]==str(position)):
                board[i][j]=symbol
#Game log
def gamelog(gamenumber,board,winner):
    global gameinfo
    gameinfo={}
    gameinfo[gamenumber]=[board,winner]
        
#Checking win
def win_check(board, symbol):
    if board[0][0] == board[0][1] == board[0][2] == symbol:
        return True
    if board[1][0] == board[1][1] == board[1][2] == symbol:
        return True
    if board[2][0] == board[2][1] == board[2][2] == symbol:
        return True
    if board[0][0] == board[1][0] == board[2][0] == symbol:
        return True
    if board[0][1] == board[1][1] == board[2][1] == symbol:
        return True
    if board[0][2] == board[1][2] == board[2][2] == symbol:
        return True
    if board[0][0] == board[1][1] == board[2][2] == symbol:
        return True
    if board[0][2] == board[1][1] == board[2][0] == symbol:
        return True
    return False
#checking winner
def play_game():
    print('Tic Tac Toe Game')   
    game=0
    winner=False
    gameno=int(input('Enter the no of games you want to play:'))
    
    while game in range(gameno):
        board_list=np.array([['0','1','2'],['3','4','5'],['6','7','8']])
        display_board(board_list)
        pchoice,cchoice=player_choice()
        winner=False
        while winner==False:
                                 
            for turn in ['Player','Computer']:
                print(turn, 'your turn!')
                if turn=='Player':
                    ppos=player_position(board_list)
                    insertt(board_list,pchoice,ppos)
                    display_board(board_list)
                    winner=win_check(board_list,pchoice)
                    if winner==True:
                        break
                else:
                    cpos=computer_position(board_list)
                    insertt(board_list,cchoice,cpos)
                    display_board(board_list)
                    winner=win_check(board_list,cchoice)
                    if winner==True:
                        break

        if winner==True  and turn=='Player':
            print('Player won!')
            win='Player'
        elif winner==True  and turn=='Computer':
            print('Computer won!')
            win='Computer' 
        else:
            print('Its a draw!')
            win='Its a draw'
       
        gamelog(game,board_list,win)   
        game+=1
 
play_game()  
while(input('Do you want to see game info-type(yes or no):').lower()=='yes') :
    gn=int(input('Enter the game no:'))
    print(gameinfo[gn][0],'\n',gameinfo[gn][1])

