import sqlite3

#connection object to represent our database
conn = sqlite3.connect(':memory:')

#cursor to execute sql commands
cu = conn.cursor()

#create doctors table
cu.execute("""CREATE TABLE doctors(doctor_id integer PRIMARY KEY,
                                   doctor_name text NOT NULL,
                                   hospital_id integer NOT NULL,
                                   joining_date text NOT NULL,
                                   speciality text NOT NULL,
                                   salary integer NOT NULL,
                                   experiance null)""")

#values
doctorinfo = [('101','David','1','2005-02-10','Pediatric','40000','NULL'),('102','Michael','1','2018-07-23','Oncologist','20000','NULL'),
('103','Susan','2','2016-05-19','Garnacologist','25000','NULL'),('104','Robert','2','2017-12-28','Pediatric','28000','NULL'),
('105','Linda','3','2004-06-04','Garnacologist','42000','NULL'),('106','William','3','2012-09-11','Dermatologist','30000','NULL'),
('107','Richard','4','2014-08-21','Garnacologist','32000','NULL'),('108','Karen','4','2011-10-17','Radiologist','30000','NULL')]

#insert values
cu.executemany("INSERT INTO doctors VALUES (?,?,?,?,?,?,?)", doctorinfo)

#create hospitals table
cu.execute("""CREATE TABLE hospitals(hospital_id integer PRIMARY KEY,
                                   hospital_name text NOT NULL,
                                   bed_count integer NOT NULL)""")

#values
hospitalinfo = [('1','Mayo Clinic','200'),('2','Cleveland Clinic','400'),('3','John Hopkins','1000'),('4','UCLA Medical Center','1500'),]

#insert values
cu.executemany("INSERT INTO hospitals VALUES (?,?,?)", hospitalinfo)

#doctors list as per input
def doclist(spec,sal):
    
    cu.execute(" SELECT * FROM doctors WHERE speciality = ? AND salary >= ? ",(spec, sal))
    item = cu.fetchall()
    print('These are the info about the doctors with the specialization you have choosen.\n')
    print('DOCTOR_ID'+'   DOCTOR_NAME'+'   HOSPITAL_ID'+'\tJOINING DATE' +'\t  SPECIALITY' +'\t    SALARY' +'\tEXPERIANCE')
    print('--------------------------------------------------------------------------------------------------------------')
    for i in item :
        print(i[0], '\t   ', i[1], '\t ', i[2], '\t\t', i[3], '\t', i[4], '\t\t', i[5], '\t\t', i[6])

#Fetch doctors as per hospital id
def dah(hosid):
    cu.execute("SELECT * FROM doctors WHERE hospital_id = ? ",(hosid,))
    item = cu.fetchall()
    cu.execute("SELECT Hospital_Name FROM hospitals WHERE Hospital_id = ?",(hosid,))
    hosname = cu.fetchone()
    print('\n')
    print('DOCTOR_ID'+'   DOCTOR_NAME'+'   HOSPITAL_ID'+'\t    HOSPITAL_NAME'+'\t    JOINING DATE' +'\tSPECIALITY' +'\t  SALARY' +'\t  EXPERIANCE')
    print('---------------------------------------------------------------------------------------------------------------------------------------')
    for i in item :
        print(i[0], '\t   ', i[1], '\t  ', i[2], '\t      ',hosname, '      ',  i[3], '      ', i[4], '\t ', i[5], '\t', i[6])

#main program

print('Doctors with the following specialization are available:Pediatric\nOncologist\nGarnacologist\nDermatologist\nRadiologist')

spec = input('Enter the specialization of the doctor from the above choices:')
sal = int(input('Enter the salary of the doctor:'))
doclist(spec,sal)
h=int(input('Enter hospital id(1,2,3,4):'))
dah(h)