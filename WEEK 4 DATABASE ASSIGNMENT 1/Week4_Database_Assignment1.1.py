import sqlite3

#connection object to represent our database
conn = sqlite3.connect(':memory:')

#cursor to execute sql commands
c = conn.cursor()

#create table
query = """CREATE TABLE CAR(
    NAME text NOT NULL,
    OWNER text NOT NULL)"""

c.execute(query)

car_list = [
    ('Audi','Anna'),('Bently','Jenny'),('Honda','Liya'),('Ferrari','John'),('Tesla','Ardra'),
    ('Nissan','Rakhi'),('Porsche','Elza'),('Volvo','Tim'),('Jaguar','Reshma'),('McLaren','Andy')
]

#inserting values into the table
c.executemany("INSERT INTO CAR VALUES (?,?)", car_list)

#selecting the values from table and printing it
c.execute("SELECT*FROM CAR")
items = c.fetchall()
print('CAR NAME' + '\tOWNER','\n----------------------')
for i in items:
    print(i[0]+ '\t\t'+ i[1])

conn.commit()
conn.close()
