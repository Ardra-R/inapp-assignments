
from random import randrange
#Creating the class pet
class Pet():
    def __init__(self, species = None ,name=""):
        species_list = ['dog', 'cat', 'horse', 'hamster']
        while species in species_list:
            self.species = species
            self.name = name
        else: print('Please enter a valid species.')

    def __str__(self,species,name):
        if name ==True:
            print("Species of: {}, named {}".format(species,name))
        else:
            print("Species of: {}, unnamed".format(species))
#Creating class Dog 
class Dog(Pet):
    def __init__(self, name= "", chases= "Cats"):
        self.chases= chases
        Pet.__init__(self, species = "Dog" ,name="")

    def __str__(self,species,name,chases):
        if name ==True:
            print("Species of: Dog, named {}, chases {}".format(name,chases))
        else:
            print("Species of: Dog, unnamed, chases {}".format(chases))

class Cat(Pet):
    def __init__(self, name= "", hates= "Dogs"):
        self.hates= hates
        Pet.__init__(self, species = "Cat" ,name="")

    def __str__(self,species,name,hates):
        if name ==True:
            print("Species of: Cat, named {}, chases {}".format(name,hates))
        else:
            print("Species of: Cat, unnamed, chases {}".format(hates))


dict ={'Dogs':[],'Cats':[]}
class Main(Dog,Cat):

    def createfivedog(self):
        
        names= ['tim','tom','cam','sam','ram']
        for i in names :
            d= Dog(i)
            dict['Dogs']=d

    def createthreecats(self):
        name = ['ann','jon','mon']
        for i in name:
            c= Cat(i)
            dict['Cats']=c

m= Main()
