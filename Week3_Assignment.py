from random import randrange
#Creating the class pet
class Pet():
    boredom_threshold = 5
    hunger_threshold = 10
    boredom_decrement = 4
    hunger_decrement = 6
    
    sounds = ['Mrrp']
    def __init__(self, name ,type):
        self.name = name
        self.type = type
        self.hunger = randrange(self.hunger_threshold)
        self.boredom = randrange(self.boredom_threshold)
        self.sounds = self.sounds[:]  # copy the class attribute, so that when we make changes to it, we won't affect the other Pets in the class

    def clock_tick(self):
        self.boredom += 1
        self.hunger += 1

    def mood(self):
        if self.hunger <= self.hunger_threshold and self.boredom <= self.boredom_threshold:
            return "happy"
        elif self.hunger > self.hunger_threshold:
            return "hungry"
        else:
            return "bored"

    def __str__(self):
        state = " I'm " + self.name + ". "
        state += " I feel " + self.mood() + ". "
        # state += "Hunger {} Boredom {} Words {}".format(self.hunger, self.boredom, self.sounds)
        print(state)
        return state
    
    def reduce_boredom(self):
        self.boredom = max(0, self.boredom - self.boredom_decrement)

    def teach(self, word):
        self.sounds.append(word)
        self.reduce_boredom()
        self.clock_tick()

    def hi(self):
        print(self.sounds[randrange(len(self.sounds))])
        self.reduce_boredom()
        self.clock_tick()

    def reduce_hunger(self):
        self.hunger = max(0, self.hunger - self.hunger_decrement)

    def feed(self):
        self.reduce_hunger()
        self.clock_tick()

    
#Main program
def main():
    while input('Do you want to interact with pets(type yes or no):').lower()  =='yes' :
        pet_type=input('Enter the type of pet you want:')
        pet_name=input('Enter the name of pet you want:')
        p=Pet(pet_name,pet_type)
        pet_list=dict()
        pet_list[pet_type]=pet_name
        print('User can type the following commands to adopt and care for the pet of your choice:\nTo quit-0 \nTo teach the pet a new word-1 \nTo interact with the pet-2 \nTo feed the pet-3 \nTo find the status of the pet-4')
        c=1
        while(c!=0):
            c=int(input('Enter your choice:'))
          
       
            if c==1: 
                w=input('Enter the word you want to teach:')
                p.teach(w)
            elif c==2:
                p.hi()
            elif c==3:
                p.feed()
            elif c==4:
                p.__str__()
            else:
                print('Goodbye')
            
        
main()